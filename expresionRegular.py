import re
class MyArray:
    def __init__(self, array):
        self.array = array
    
    def new (array):
        return MyArray(array)
    
    def operation(self):
        if (len(re.findall('(\()', self.array)) != len(re.findall('(\))', self.array))):
            return False
        if (len(re.findall('[a-z]|[A-Z]', self.array))>0):
            return False
        if (len(re.findall('\(\)|[\+|\-|\*|/]\)|\)\d', self.array))>0):
            return False
        return True
    
    def compute(self):
        if self.operation():
            return eval(self.array)
        else:
            return False


o=MyArray.new("((2-1)/(2-1))")
print(o.compute())

