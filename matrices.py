class MyMAtrix:

    def __init__(self, matrix):
        self.matrix = matrix
    
    def new(matrix):
        return MyMAtrix(matrix)
    
    def compute(self):
        auxList = [self.matrix]
        total = 0
        for item in auxList:
            for i in range(len(item)):
                if type(item[i]) == list:
                    auxList.append(item[i])
                elif type(item[i]) == int:
                    total+=item[i]
        return total
    
    def straigth(self):
        auxList = [(self.matrix, 0, len(self.matrix))]
        lastLevel = 0
        for item, level, numItems in auxList:
            for i in range(len(item)):
                if type(item[i]) == list:
                    auxList.append((item[i], level+1, len(item[i])))
                    if lastLevel < level + 1:
                        lastLevel = level+1
        
        allEqual = True
        for i in range(lastLevel+1):
            aux=-1
            for item, level, numItems in auxList:
                if level == i:
                    if aux == -1:
                        aux = numItems
                    elif aux != numItems:
                        allEqual = False

        return allEqual

    def dimension(self):
        depth = 1
        auxList = [(self.matrix, 0)]
        lastLevelAdded = -1
        for item, level in auxList:
            auxLevel=False
            for i in range(len(item)):
                if type(item[i]) == list:
                    auxList.append((item[i], level+1))
                    auxLevel=True
            if auxLevel and lastLevelAdded != level:
                depth= depth + 1
                lastLevelAdded = level
        return depth
        
matrix =  [[[1, 2, 3]], [[5, 6, 7], [5, 4, 3]], [[3, 5, 6], [4, 8, 3], [2, 3]]]


o = MyMAtrix.new(matrix)
print(o.compute())